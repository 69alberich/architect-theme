$("document").ready(function(){

  $('#payment-form').on('submit', function(e){
    e.preventDefault(); 
    var request = $(this).payStripe();
    
      request.then(function(response){
      var respuesta = JSON.parse(response.result);
      console.log("respuesta", respuesta);
      if(respuesta.status === "succeeded"){
         alert("Pago completado satisfactoriamente");
         location.reload();
      }else{
          //console.log(response);
          alert("Ha ocurrido un error:"+respuesta.message);
         //location.reload();
      }
      },
      function(response){
        alert("Ha ocurrido un error");
        //location.reload();
      });
      
  });
});