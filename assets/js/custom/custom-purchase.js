
$(document).ready(function(){

    new ClipboardJS('.clipboard-trigger');

    $(".input-mask-trigger").inputmask();


    $(".logout").on("click", function(){
        $.request('Logout::onAjax',
            {success: function(data){
                //console.log(data);
                location.reload();
            },
            
            error: function(data){  
                //console.log(data);
                location.reload();
            }
        
        });
    });

   /* $(".app-container").on("submit", ".add-cart-form", function(e){
        e.preventDefault();
        var form = $(this).serializeArray();
        var formData = convertToObject(form);
        var data = {
            'cart': [
                {'offer_id': formData.offer, 'quantity': formData.quantity}
            ],
            'shipping_type_id': 1
        };
        $.request('Cart::onAdd', {
            'data': data,
            'update': {
                'cart/cart-aside': '#cart-aside-container',
            }
        }).then(function(){
            toastr.success('¡Correcto!', 'Producto añadido al carrito', 
            {timeOut: 1000, positionClass: 'toast-bottom-right'});
            
        });
    });

    $("#cart-aside-container").on("click", ".remove-from-cart", function(){
        var offerId = $(this).data("offer-id");
        var data = {
            'cart': [offerId],
            'shipping_type_id': 1
        };
        
        //Send ajax request and update cart items
        $.request('Cart::onRemove', {
            'data': data,
            'update': {
                'cart/cart-aside': '#cart-aside-container',
            }
        });
    });*/

    $(".next-btn").on("click", function () {
        // Navigate next
        $('#smartwizard').smartWizard("next");
        return true;
    });

    $(".prev-btn").on("click", function () {
        $('#smartwizard').smartWizard("prev");
        return true;
    });

    $("#order-form").on("submit", function(e){
        e.preventDefault();
        var  form = $(this).serializeArray();
        var formData = convertToObject(form);
        console.log(formData);
        
        $(this).request('CartSessionList::onCreateOrder',{
            success: function(data){
                //console.log(data);
                if(data["error"]){
                    toastr.error(data["error"][0], "Espera", 
                    {timeOut: 1500, positionClass: 'toast-bottom-right'});
                }else{
                    //$(location).attr('href', 'http://localhost/dictya/order/'+data.secret);
                    $(location).attr('href', 'https://dictya.com/order/'+data.secret_key);
                }
                
            }
        });
        

        /*var data = {
            'order': {
                'shipping_type_id': 1,
                'property':{
                    'document': formData.document,
                }
            },
            'user': {
                'email': formData.email,
                'name': formData.name,
                'last_name':formData.lastname,
            },

        }*/
        
        /*$(this).request('MakeOrder::onCreate', {
            //'data': data,
            'success': function(data){
                //redirect a data.key
                console.log(data);
                //alert(datakey);
                $(location).attr('href', 'https://dictya.com/moon/order/'+data.data.key);
                //window.location.href = "http://localhost/sandbox/orden/"+data[key;
            }
        });*/
    });

    $("#approve-button").on("click", function(){
        var code = $(this).data("code");

        $.request("onApprove",{
            data: {code: code},
            success: function(response){
                Swal.fire({
                    type: 'success',
                    title: 'Bien',
                    text: "¡Orden aprobada correctamente!",
                }).then(function(){
                    location.reload();
                });
            },
            error: function(response){
                //console.log(response.responseJSON.message);
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: response.responseJSON.message,
                }).then(function(){
                    location.reload();
                });
            },
        })
    });

    //////////////////////////////////////////NUEVO////////////////////////////////////////

    $(".app-container").on("submit", ".chargeForm", function(e){
       
        e.preventDefault();
        //var form = $(this).serializeArray();chargeModalLabel
        //var formData = convertToObject(form);

        $(this).request('CartSessionList::onAddCharge',{
            'update': {
                'cart/cart-aside': '#cart-aside-container',
                'step/charge-list-step': '#charge-list-step-container',
            }, success: function(response){
                
                if(response["error"]){
                    toastr.error(response["error"][0], "Espera", 
                    {timeOut: 1500, positionClass: 'toast-bottom-right'});
                }else{
                    toastr.success('¡Correcto!', 'Cargo añadido a la orden', 
                    {timeOut: 1000, positionClass: 'toast-bottom-right'});
                    $('#chargeModal').modal('hide');
                
                }
                this.success(response);
                //console.log(response);
                
            },
            
        }).then(()=>{
            $("#chargeModal").modal("hide");
        });
    });

    $("#charge-list-step-container").on("click", ".close", function(e){
        $.request("CartSessionList::onRemoveCharge", {
            data:{
                index: $(this).data("index")
            },
            'update': {
                'cart/cart-aside': '#cart-aside-container',
                'step/charge-list-step': '#charge-list-step-container',
            }
        });
        //alert("tengo:"+$(this).data("index"));
    });

    $("body").on("click", "#addChargeButton", function(){
        $("#chargeModal").modal("show");
    })
});

