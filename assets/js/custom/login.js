$(document).ready(function(){

    var translator = $('body').translate({lang: "es", t: dict});


    function validateChangePassword(data){
        var errorMessage ="";
    
        if(data["old_password"]===""){
            errorMessage+="*Escribe tu contraseña anterior <br>";
        }
        if(data["password"]===""){
            errorMessage+="*Tu nueva contraseña es requerida <br>";
        }
    
        if(data["password_confirmation"]==="" || data["password_confirmation"] !== data["password"]){
            errorMessage+="*la confirmación de contraseña es requerida y debe ser igual al a nueva contraseña <br>";
        }
    
    
        return errorMessage;
    }
    
    
    $('.dropify').dropify({
        messages: {
            'default': 'arrastra o haz click aqui para adjuntar tu comprobante',
            'replace': 'arrastra y suelta tu comprobante para reemplazarlo',
            'remove':  'remover',
            'error':   'Algo ha salido mal'
        }
    });

    zelleFlag = true;

    $("#login-form").on("submit", function(e){
        e.preventDefault();

        $(this).request('Login::onAjax', {
            success: function(data){
                console.log(data);
                if(data.status == false){
                    toastr.warning(translator.get(data.message), 'Espera' );
                }else{
                    //header.js
                    this.success(data);
                }

            },
            
            error: function(data){  
                toastr.error(translator.get(data.message), 'Ha ocurrido un error' );
            }
        });
        //console.log(data);
    });

    $(".notification-wrapper").on("click", "#logout", function(){
        $.request('Logout::onAjax',
            {success: function(data){
                location.reload();
            },
            
            error: function(data){  
                location.reload();
            }
        
        });
    });

    $('.app-container').on('submit', "#payment-form", function(e){
        e.preventDefault();
        var   request = $(this).payStripe();
        request.then(function(response){
            var respuesta = JSON.parse(response.result);
            if(respuesta.status === "succeeded"){
                //abre aqui
                var alerta_stripe = swal({
                title: 'Hemos procesado su pago correctamente',
                text: 'Espera unos segundos mientras terminamos el proceso',
                closeOnEsc:false,
                allowEnterKey: false,
                allowOutsideClick: false,
                button:false,
                timer: 2500,
                }).then( response => {
                    var data ={
                        reference: respuesta["reference"],
                        method_id: 1,
                        currency: 1,
                        status_id: 1,
                        order_status_id: 3,
                    }
                    $.request('PaymentsHandler::onAddPayment', {
                        data: data,
                        success: function(response) {
                            swal.close();
                            swal({
                            title: '¡Proceso Completado!',
                            text: 'Gracias usar nuestro servicio',
                            
                            }).then(function(){
                                location.reload();
                            }
    
                            );
                        },
                        error: function(response){
                        swal.close();
                        swal({
                        
                            title: 'Ha ocurrido un error',
                            text: 'Intenta nuevamente por favor',
                            
                        }).then(function(){
                            location.reload();
                        });
                        }
                    });
    
                },
                function(){
                    swal.close();
                    swal({
                    
                        title: 'Ha ocurrido un error',
                        text: 'Intenta nuevamente por favor',
                        
                    }).then(function(){
                        location.reload();
                    });
                });
                //termina aqui
                
            }else{
                swal({
                    title: 'Ha ocurrido un error',
                    text: "Intenta nuevamente por favor"
                }).then(function(){
                    location.reload();
                });
            }
        }).fail(function(response){
            //console.log("falle por aqui", response);
            swal({
                title: 'Ha ocurrido un error code(500)',
                text: "Intenta nuevamente por favor",
            }).then(function(){
                location.reload();
            });
        });

    });

    $('.app-container').on('submit', "#payment-form-zelle", function(e){
        e.preventDefault();
        
        var data ={
            method_id: 2,
            status_id: 1,
            order_status_id: 2,
        }
        if (zelleFlag) {
            zelleFlag = false;
            $(this).request('PaymentsHandler::onAddPayment', {
                data: data,
                'success': function(response){
                  
                    if(response["error"]){
                        toastr.error(response["error"][0], "Espera", 
                        {timeOut: 1500, positionClass: 'toast-bottom-right'});
                    }else{
                        swal({
                            title: "¡Bien hecho!",
                            text: "Hemos recibido tu comprobante",
                            icon: "success",
                            button: "aceptar",
                          }).then((value)=>{
                              location.reload();
                          });
                    }
                    
                },
                'error': function(response){
                    console.log(response);
                    swal({
                        title: "¡Oops!",
                        text: "Ha ocurrido un error, intenta nuevamente",
                        icon: "error",
                        button: "aceptar",
                      }).then((value)=>{
                          location.reload();
                      });
                },
                complete: function(){
                    zelleFlag = true;
                }
            });
        }else{
            //console.log("no puedes mandar mas wong");
        }
        
    });
    

    $("body").on("submit", "#restore-form", function(e){
        e.preventDefault(); 

        $(this).request('RestorePassword::onAjax',{

            success: function(response){
                if (response.status == true) {
                    swal({
                        title: '¡Correcto!',
                        text: 'En breve recibirás un correo con instrucciones para reestablecer tu cuenta',
                        button:true,
                      });
                }else{
                    swal({
                        icon: 'warning',
                        title: '¡UPS!',
                        text: 'No hemos localizado tu correo en nuestra base de datos, verificalo',
                        button:true,
                    });
                }
                
            },
            error: function(response){
                swal({
                    icon: 'warning',
                    title: '¡UPS!',
                    text: 'No hemos localizado tu correo en nuestra base de datos, verificalo',
                    button:true,
                });
            }
        });
    });

    $("body").on("submit", "#reset-form", function(e){
        e.preventDefault();
        var message = "";
        var form = $(this).serializeArray();
        //console.log(data);
        var data = convertToObject(form);

        var message = validateChangePassword(data);
        if (message =="") {
            $(this).request('ResetPassword::onAjax',{

                success: function(response){
                    if (response.status == true) {
                        swal({
                            title: '¡Correcto!',
                            text: 'Has modificado tu contraseña correctamente, ahora puedes iniciar sesión',
                            button:true,
                          }).then(()=>{
                            location.href = "https://dictya.com";
                          });
                    }else{
                        console.log(response);
                        swal({
                            icon: 'warning',
                            title: '¡UPS!',
                            text: 'Ha ocurrido un error:'+response.message,
                            button:true,
                        });
                    }
                    
                },
                error: function(response){
                    console.log(response);
                    swal({
                        icon: 'warning',
                        title: '¡UPS!',
                        text: 'Ha ocurrido un error, intenta nuevamente o comunicate con nosotros al chat',
                        button:true,
                    });
                }
            });
        }else{
            toastr.warning(message, 'Espera' );
        }

        
    });
});



